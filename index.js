let activeLetter = '';
const tabArray = ['Enter', 'S', 'E', 'O', 'N', 'L', 'Z'];
document.addEventListener('keydown', (e) => {
    document.querySelector('.btn-active')?.classList.add('btn');
    document.querySelector('.btn-active')?.classList.remove('btn-active');
    if (tabArray.includes(e.code.replaceAll('Key', ''))) {
        activeLetter = e.code.replaceAll('Key', '')
    };   
    document.querySelector(`.${activeLetter}.btn`).classList.remove('btn');
    document.querySelector(`.${activeLetter}`).classList.add('btn-active');
});





